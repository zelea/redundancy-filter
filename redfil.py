import sys
import os
import xxhash

def eprint(*args, **kvargs) -> None:
    print(*args, **kvargs, file= sys.stderr)

eprint("Listing files...")
files = []
if len(sys.argv) != 2:
    folder = '.'
else:
    folder = sys.argv[1]

for root, _, dfiles in os.walk(folder):
    for file in  dfiles:
        files.append(os.path.join(root, file))
        eprint(len(files), end= '\r')

eprint()

hashes = []

eprint("Calculating hashes...")
for file in files:
    try:
        with open(file, 'rb') as f:
            hashes.append((xxhash.xxh3_128(f.read()).hexdigest(), file))
            eprint(f"{len(hashes)}/{len(files)}", end='\r')
    except Exception as e:
        eprint(e)

hashes.sort(key=lambda x: x[0])

redundants = []

eprint("Looking for redundancy...")
for i in range(len(hashes)-1):
    if hashes[i][0] == hashes[i+1][0]:
        if hashes[i] not in redundants:
            redundants.append(hashes[i])
        redundants.append(hashes[i+1])

print(*[': '.join(redundant) for redundant in redundants], sep='\n')